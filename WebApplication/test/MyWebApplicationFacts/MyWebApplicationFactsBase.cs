﻿using System.Net.Http;
using System.Web.Http;
using MyWebApplication;
using Xunit.Abstractions;

namespace MyWebApplicationFacts
{
    public class MyWebApplicationFactsBase
    {
        protected ITestOutputHelper output;
        protected HttpConfiguration httpConfiguration;
        protected HttpServer httpServer;
        protected MyWebApp myApp;
        protected HttpClient httpClient { get; set; }

        public MyWebApplicationFactsBase()
        {
            httpConfiguration = new HttpConfiguration();
            myApp = new MyWebApp();
            myApp.CreateGetMessageWebApi(httpConfiguration);
            myApp.CreatePostMessageWebApi(httpConfiguration);
            myApp.CreateUrlHasParametersWebApi(httpConfiguration);
            myApp.CreateSendAnanimousObjectWebApi(httpConfiguration);
            myApp.CreateUrlHasQueryWebApi(httpConfiguration);
            httpServer = new HttpServer(httpConfiguration);
            httpClient = new HttpClient(httpServer);
        }
    }
}