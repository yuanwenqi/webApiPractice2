﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using MyWebApplication;
using MyWebApplication.controllers.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Xunit;
using Xunit.Abstractions;
using Formatting = Newtonsoft.Json.Formatting;

namespace MyWebApplicationFacts
{
    public class MyWebApplicationFacts : MyWebApplicationFactsBase
    {
        readonly SerializerByXml serializerByXml;

        public MyWebApplicationFacts(ITestOutputHelper output)
        {
            this.output = output;
            serializerByXml = new SerializerByXml();
        }

        [Fact]
        public async Task should_return_hello_message()
        {
            var responseMessage = await httpClient.GetAsync("http://baidu.com/message");
            Assert.Equal(HttpStatusCode.OK, responseMessage.StatusCode);
            var content = await responseMessage.Content.ReadAsAsync<string>();
            MessageDto DeserializeContent = JsonConvert.DeserializeObject<MessageDto>(content);
            Assert.Equal("hello", DeserializeContent.message);
        }

        [Fact]
        public async Task should_return_username_and_password()
        {
            var responseMessage = await httpClient.GetAsync("http://baidu.com/Login/coco&hellococo");
            Assert.Equal(HttpStatusCode.OK, responseMessage.StatusCode);
            var content = await responseMessage.Content.ReadAsAsync<string>();
            output.WriteLine(content);
            UserInfo DeserializeContent = JsonConvert.DeserializeObject<UserInfo>(content);
            Assert.Equal("coco", DeserializeContent.username);
            Assert.Equal("hellococo", DeserializeContent.password);
        }


        [Fact]
        public async Task should_post_username_and_password_query_string_from_uri()
        {
            HttpResponseMessage response = await httpClient.PostAsync(
                new Uri("http://baidu.com/LoginWithQuery/?username=coco&password=hellococo"),
                default(StringContent));
            var content = await response.Content.ReadAsStringAsync();
            output.WriteLine(content);
            var DeserializeContent = JsonConvert.DeserializeObject<UserInfo>(content);
            Assert.Equal("coco", DeserializeContent.username);
            Assert.Equal("hellococo", DeserializeContent.password);
            
        }

        [Fact]
        public void should_serialize_object_by_json()
        {
            var userInfo = new UserInfo("Name", "password");
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            string serializeObject = JsonConvert.SerializeObject(userInfo, Formatting.None, settings);
            output.WriteLine(serializeObject);
        } 
        
        [Fact]
        public void should_Deserialize_object_by_json()
        {
            string userInfo = "{ \"username\":\"name\",\"password\":\"password1\"}";
            var serializeObject = JsonConvert.DeserializeObject<UserInfo>(userInfo);
            Assert.Equal("name",serializeObject.username);
            Assert.Equal("password1",serializeObject.password);
        }

        [Fact]
        public void should_serialize_and_deserialize_object_by_xml()
        {
            var userInfo = new UserInfo("name", "password");
            string serialize = serializerByXml.Serialize(userInfo);
            output.WriteLine(serialize);
            var deserialize = serializerByXml.Deserialize<UserInfo>(serialize);
            Assert.Equal("name",deserialize.username);
            Assert.Equal("password",deserialize.password);
        }

        [Fact]
        public async Task should_post_content_with_anonymousType()
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, "http://baidu.com/message");
            httpRequestMessage.Content = new StringContent(
                JsonConvert.SerializeObject(new {message = "hello"}),
                    Encoding.UTF8,
                    "application/json"
                );
            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(
                httpRequestMessage);
            Assert.Equal(HttpStatusCode.OK, httpResponseMessage.StatusCode);
            var json = await httpResponseMessage.Content.ReadAsStringAsync();
            var content = JsonConvert.DeserializeAnonymousType(json, new {message = default(string)});
            Assert.Equal("hello",content.message);
        }

        [Fact]
        public async Task should_post_content_with_anonymousType_2_paramaters()
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, "http://baidu.com/Login2");
            httpRequestMessage.Content = new StringContent(
                JsonConvert.SerializeObject(new {username ="user",password = "password"}),
                    Encoding.UTF8,
                    "application/json"
                );
            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(
                httpRequestMessage);
            Assert.Equal(HttpStatusCode.Created, httpResponseMessage.StatusCode);
            var json = await httpResponseMessage.Content.ReadAsStringAsync();
            var content = JsonConvert.DeserializeAnonymousType(json, new {username = default(string),password = default(string)});
            Assert.Equal("user",content.username);
            Assert.Equal("password", content.password);
        } 
        
        [Fact]
        public async Task should_post_userinfo_content_with_Object_by_json()
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, "http://baidu.com/Login2");
            httpRequestMessage.Content = new StringContent(
                JsonConvert.SerializeObject(new UserInfo("name","pass")),
                    Encoding.UTF8,
                    "application/json"
                );
            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(
                httpRequestMessage);
            Assert.Equal(HttpStatusCode.Created, httpResponseMessage.StatusCode);
            var json = await httpResponseMessage.Content.ReadAsStringAsync();
            var content = JsonConvert.DeserializeObject<UserInfo>(json);
            Assert.Equal("name",content.username);
            Assert.Equal("pass", content.password);
        } 

        [Fact]
        public async Task should_post_userinfo_content_with_Object_by_xml()
        {

           
            string serializeXml = serializerByXml.Serialize(new UserInfo("name", "pass"));
            output.WriteLine(serializeXml);
            HttpResponseMessage httpResponseMessage = await httpClient.PostAsXmlAsync("http://baidu.com/Login2",serializeXml);
            Assert.Equal(HttpStatusCode.Created, httpResponseMessage.StatusCode);
            string xml = await httpResponseMessage.Content.ReadAsStringAsync();
            output.WriteLine(xml);

            // cannot remove d1p1

//            var content = serializerByXml.Deserialize<UserInfo>(xml);
//            Assert.Equal("name",content.username);
//            Assert.Equal("pass", content.password);
        }



    }
}