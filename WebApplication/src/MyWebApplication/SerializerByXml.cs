﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace MyWebApplication
{
    public class SerializerByXml
    {
        public SerializerByXml()
        {
        }

        public string Serialize<T>(T obj) where T : class
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter))
            {
                serializer.Serialize(writer, obj);
                return stringWriter.ToString();
            }
        }

        public T Deserialize<T>(string xml) where T : class
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(xml))
            {
                object deserializationObj = deserializer.Deserialize(reader);
                return deserializationObj as T;
            };
        }
    }
}