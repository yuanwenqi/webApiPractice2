﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace MyWebApplication
{
    public class MyWebApp
    {
        public void CreateGetMessageWebApi(HttpConfiguration configuration)
        {
            var routes = configuration.Routes;
            routes.MapHttpRoute(
                "return hello",
                "message",
                new {controller = "Message", action = "GetMessage"},
                new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)});
            configuration.EnsureInitialized();
        }

        public void CreatePostMessageWebApi(HttpConfiguration configuration)
        {
            var routes = configuration.Routes;
            routes.MapHttpRoute(
                "post hello",
                "message",
                new {controller = "Message", action = "PostMessage"},
                new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)});
            configuration.EnsureInitialized();
        }

        public void CreateUrlHasParametersWebApi(HttpConfiguration configuration)
        {
            var routes = configuration.Routes;
            routes.MapHttpRoute(
                "send request",
                "login/{username}&{password}",
                new {controller = "Login", action = "Login"},
                new {HttpMethod = new HttpMethodConstraint(HttpMethod.Get)});
            configuration.EnsureInitialized();
        }
        public void CreateUrlHasQueryWebApi(HttpConfiguration configuration)
        {
            var routes = configuration.Routes;
            routes.MapHttpRoute(
                "send request WithQuery",
                "LoginWithQuery",
                new {controller = "Login", action = "LoginWithQuery"},
                new {HttpMethod = new HttpMethodConstraint(HttpMethod.Post)});
            configuration.EnsureInitialized();
        }

        public void CreateSendAnanimousObjectWebApi(HttpConfiguration configuration)
        {
            var routes = configuration.Routes;
            routes.MapHttpRoute(
                "send request2",
                "Login2",
                new {controller = "Login", action = "Login2"},
                new {HttpMethod = new HttpMethodConstraint(HttpMethod.Post)});
            configuration.EnsureInitialized();
        }
    }
}