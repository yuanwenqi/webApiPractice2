﻿using System;

using System.Web.Http;


namespace MyWebApplication
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            new MyWebApp().CreateGetMessageWebApi(GlobalConfiguration.Configuration);
            new MyWebApp().CreatePostMessageWebApi(GlobalConfiguration.Configuration);
        }
    }
}