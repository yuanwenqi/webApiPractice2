﻿
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MyWebApplication.controllers.Dto;
using Newtonsoft.Json;

namespace MyWebApplication.controllers
{
    public class MessageController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage GetMessage()
        {
            return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(new MessageDto("hello"),Formatting.Indented));
        }

        [HttpPost]
        public HttpResponseMessage PostMessage([FromBody]object obj)
        {
            return Request.CreateResponse(HttpStatusCode.OK,obj);
        }

    }
}
