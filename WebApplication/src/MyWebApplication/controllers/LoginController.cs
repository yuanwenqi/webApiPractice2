﻿
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MyWebApplication.controllers.Dto;
using Newtonsoft.Json;

namespace MyWebApplication.controllers
{
    public class LoginController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Login(
            [FromUri] string Username,
            [FromUri] string Password)
        {
            return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(new UserInfo(Username,Password),Formatting.None));
        }

        [HttpPost]
        public HttpResponseMessage Login2([FromBody]object obj)
        {
            return Request.CreateResponse(HttpStatusCode.Created,obj );
        }

        [HttpPost]
        public HttpResponseMessage LoginWithQuery([FromUri] UserInfo userInfo)
        {
            return Request.CreateResponse(HttpStatusCode.OK,userInfo);
        }


    }
}
