﻿namespace MyWebApplication.controllers.Dto
{
    public class MessageDto
    {
        public string message
        {
            get; 
            set;
        }

        public MessageDto(string m)
        {
            message = m;
        }
    }
}