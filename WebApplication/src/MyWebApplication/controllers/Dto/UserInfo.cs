﻿namespace MyWebApplication.controllers.Dto
{
    public class UserInfo
    {
        public string username { get; set; }
        public string password { get; set; }

        public UserInfo() {
        }

        public UserInfo(string Username, string Password)
        {
            username = Username;
            password = Password;

        }
    }
}